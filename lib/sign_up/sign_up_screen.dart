import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:gym_match/sign_in/sign_in_screen.dart';
import 'package:gym_match/sign_up/emailVerification.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:gym_match/utils/api_helper.dart';
import 'package:internet_connection_checker/internet_connection_checker.dart';

class SignupScreen extends StatefulWidget {
  const SignupScreen({Key? key}) : super(key: key);

  @override
  _SignupScreen createState() => _SignupScreen();
}

class _SignupScreen extends State<SignupScreen> {
  int? password;
  final _formKey = GlobalKey<FormState>();
  final items = ['Male', 'Female', 'Non-binary'];
  final _apiBaseHelper = ApiBaseHelper();
  String? value;
  bool isLoading = false;
  bool hasInternet = true;
  String email = '';
  String? phoneNumber;
  String? name;
  String? lastName;
  String? birthday;
  String? confPassword;
  String? gender;
  bool signUp = false;

  Future<void> sharedPreferences() async {
    final prefs = await SharedPreferences.getInstance();
    await prefs.setString('email', email);
  }

  Future<void> hasInternetMethod() async {
    hasInternet = await InternetConnectionChecker().hasConnection;
    if (hasInternet == false) {
      showAboutDialog(
          context: context,
          applicationName: 'GyMatch',
          applicationIcon: SvgPicture.asset('assets/images/close_icon.svg'),
          children: <Widget>[const Text('Check your internet connection.')]);
    }
    setState(() => isLoading = false);
  }

  String getEmail() {
    return email;
  }

  String getGender() {
    String string = value![0];
    return string;
  }

  Future<void> getSignupInfo() async {
    gender = getGender();
    email = getEmail();
    sharedPreferences();
    var response = await _apiBaseHelper.postHTTPDev('/sign-up', {
      "email": email,
      "phone_number": phoneNumber,
      "names": name,
      "family_names": lastName,
      "gender": gender,
      "born_at": birthday,
      "password": confPassword
    });
    if (response.data != null && response.status == 200) {
      signUp = true;
      setState(() => isLoading = false);
    } else {
      setState(() => isLoading = false);
    }
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Scaffold(
      body: SizedBox(
        width: double.infinity,
        height: size.height,
        child: Form(
          key: _formKey,
          child: SingleChildScrollView(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: <Widget>[
                const TextFieldContainer(
                    key: null,
                    child: Text('GyMatch',
                        textAlign: TextAlign.center,
                        style: TextStyle(
                            fontWeight: FontWeight.bold,
                            fontSize: 60,
                            color: Color.fromRGBO(87, 131, 188, 1.0)))),
                const TextFieldContainer(
                    key: null,
                    child: Text('Sign up',
                        textAlign: TextAlign.center,
                        style: TextStyle(
                            fontWeight: FontWeight.bold, fontSize: 30))),
                TextFieldContainer(
                    key: null,
                    child: TextFormField(
                      keyboardType: TextInputType.phone,
                      decoration: const InputDecoration(
                        hintText: "Phone number",
                      ),
                      inputFormatters: [
                        LengthLimitingTextInputFormatter(10),
                        FilteringTextInputFormatter.digitsOnly,
                      ],
                      onSaved: (val) {},
                      validator: (val) {
                        phoneNumber = val;
                        if (val!.isEmpty == true) {
                          return 'Please, type your phone number.';
                        } else if (val.length < 10) {
                          return 'Your phone number cannot be that short.';
                        } else {
                          return null;
                        }
                      },
                    )),
                TextFieldContainer(
                    key: null,
                    child: TextFormField(
                      keyboardType: TextInputType.name,
                      decoration: const InputDecoration(
                        hintText: "First Name",
                      ),
                      inputFormatters: [
                        FilteringTextInputFormatter.singleLineFormatter,
                      ],
                      onSaved: (val) {},
                      validator: (val) {
                        name = val;
                        if (val!.isEmpty == true) {
                          return 'Please, type your first name.';
                        } else if (val.contains(RegExp(r'[0-9]'))) {
                          return 'That is not a valid name.';
                        } else {
                          return null;
                        }
                      },
                    )),
                TextFieldContainer(
                    key: null,
                    child: TextFormField(
                      keyboardType: TextInputType.name,
                      decoration: const InputDecoration(
                        hintText: "Last Name",
                      ),
                      inputFormatters: [
                        FilteringTextInputFormatter.singleLineFormatter,
                      ],
                      onSaved: (val) {},
                      validator: (val) {
                        lastName = val;
                        if (val!.isEmpty == true) {
                          return 'Please, type your last name.';
                        } else if (val.contains(RegExp(r'[0-9]'))) {
                          return 'That is not a valid name.';
                        } else {
                          return null;
                        }
                      },
                    )),
                TextFieldContainer(
                    key: null,
                    child: TextFormField(
                      keyboardType: TextInputType.emailAddress,
                      decoration: const InputDecoration(
                        hintText: "Email address",
                      ),
                      inputFormatters: [
                        FilteringTextInputFormatter.singleLineFormatter,
                      ],
                      onSaved: (val) {},
                      validator: (val) {
                        email = val!;
                        if (val.isEmpty == true) {
                          return 'Please, type your email address.';
                        } else if (val.contains('@') == false ||
                            val.contains('.') == false) {
                          return 'That is not a valid email address.';
                        } else {
                          return null;
                        }
                      },
                    )),
                TextFieldContainer(
                    key: null,
                    child: TextFormField(
                      decoration: const InputDecoration(
                        hintText: "Birthday (YYYY-MM-DD)",
                      ),
                      inputFormatters: [
                        LengthLimitingTextInputFormatter(10),
                      ],
                      keyboardType: TextInputType.datetime,
                      onSaved: (val) {},
                      validator: (val) {
                        birthday = val;
                        if (val!.isEmpty == true) {
                          return 'Please, type your birthday.';
                        } else if (val.length < 10) {
                          return 'Use the right format (YYYY-MM-DD).';
                        } else if (val.contains('-') == false) {
                          return 'Use the right format (YYYY-MM-DD).';
                        } else {
                          return null;
                        }
                      },
                    )),
                TextFieldContainer(
                    key: null,
                    child: DropdownButton<String>(
                      value: value,
                      iconSize: 40,
                      hint: const Text('Gender'),
                      isExpanded: true,
                      items: items.map(buildMenuItem).toList(),
                      onChanged: (value) => setState(() => this.value = value),
                    )),
                TextFieldContainer(
                  key: null,
                  child: TextFormField(
                      keyboardType: TextInputType.number,
                      obscureText: true,
                      decoration: const InputDecoration(
                        hintText: "Password",
                      ),
                      inputFormatters: [
                        LengthLimitingTextInputFormatter(6),
                        FilteringTextInputFormatter.digitsOnly,
                      ],
                      onSaved: (val) {
                        password = int.parse(val!);
                      },
                      validator: (val) {
                        confPassword = val;
                        password = int.parse(val!);
                        if (val.isEmpty == true) {
                          return 'Please, type a password.';
                        } else if (val.length < 6) {
                          return 'The password is too short.';
                        } else {
                          return null;
                        }
                      }),
                ),
                TextFieldContainer(
                  key: null,
                  child: TextFormField(
                    keyboardType: TextInputType.number,
                    obscureText: true,
                    decoration: const InputDecoration(
                      hintText: "Confirm password",
                    ),
                    inputFormatters: [
                      LengthLimitingTextInputFormatter(6),
                      FilteringTextInputFormatter.digitsOnly,
                    ],
                    onSaved: (val) {},
                    validator: (val) {
                      confPassword = val;
                      if (val!.isEmpty == true) {
                        return 'Please, confirm your password.';
                      } else if (val.compareTo(password.toString()) != 0) {
                        return 'The password do not match.';
                      } else {
                        return null;
                      }
                    },
                  ),
                ),
                SizedBox(
                  width: size.width * 0.8,
                  child: ClipRRect(
                    borderRadius: BorderRadius.circular(29),
                    child: ElevatedButton(
                      style: ButtonStyle(
                          backgroundColor: MaterialStateProperty.all<Color>(
                              const Color.fromRGBO(87, 131, 188, 1.0))),
                      child: isLoading
                          ? const CircularProgressIndicator(
                              color: Colors.white, strokeWidth: 2.0)
                          : const Text('GET STARTED',
                              style: TextStyle(color: Colors.white)),
                      onPressed: () async {
                        if (!_formKey.currentState!.validate()) {
                          return;
                        }
                        setState(() => isLoading = true);
                        await hasInternetMethod();
                        await getSignupInfo();
                        if (signUp == true) {
                          Navigator.push(
                            context,
                            MaterialPageRoute(builder: (context) {
                              return const EmailVerificationScreen();
                            }),
                          );
                        } else if (signUp == false && hasInternet == true) {
                          showAboutDialog(
                              context: context,
                              applicationName: 'GyMatch',
                              applicationIcon: SvgPicture.asset(
                                  'assets/images/close_icon.svg'),
                              children: <Widget>[
                                const Text(
                                    'Phone number and/or email have already been used.')
                              ]);
                        }
                      },
                    ),
                  ),
                ),
                SizedBox(
                  width: size.width * 0.8,
                  child: ClipRRect(
                    borderRadius: BorderRadius.circular(29),
                    child: ElevatedButton(
                      style: ButtonStyle(
                          backgroundColor: MaterialStateProperty.all<Color>(
                              const Color.fromRGBO(87, 131, 188, 1.0))),
                      child: isLoading
                          ? const CircularProgressIndicator(
                              color: Colors.white, strokeWidth: 2.0)
                          : const Text('Already Signed in',
                              style: TextStyle(color: Colors.white)),
                      onPressed: () => Navigator.push(context,
                          MaterialPageRoute(builder: (context) {
                        return const SigninScreen();
                      })),
                    ),
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }

  DropdownMenuItem<String> buildMenuItem(String item) =>
      DropdownMenuItem(value: item, child: Text(item));
}

class TextFieldContainer extends StatelessWidget {
  final Widget child;
  const TextFieldContainer({
    required Key? key,
    required this.child,
  }) : super(key: key);
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Container(
      margin: const EdgeInsets.symmetric(vertical: 10),
      padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 5),
      width: size.width * 0.8,
      decoration: BoxDecoration(
          color: Colors.white, borderRadius: BorderRadius.circular(29)),
      child: child,
    );
  }
}
