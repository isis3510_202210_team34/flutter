import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:gym_match/sign_in/sign_in_screen.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:gym_match/utils/api_helper.dart';
import 'package:internet_connection_checker/internet_connection_checker.dart';

class EmailVerificationScreen extends StatefulWidget {
  const EmailVerificationScreen({Key? key}) : super(key: key);

  @override
  _EmailVerificationScreen createState() => _EmailVerificationScreen();
}

class _EmailVerificationScreen extends State<EmailVerificationScreen> {
  final _formKey = GlobalKey<FormState>();
  final _apiBaseHelper = ApiBaseHelper();
  bool isLoading = false;
  bool isLoading2 = false;
  bool hasInternet = true;
  String email = '';
  String? otp;
  String? pin1;
  String? pin2;
  String? pin3;
  String? pin4;
  String? pin5;
  String? pin6;
  bool isCode = false;
  bool reintentar = false;

  String getOtp() {
    String string = "000000";
    if (pin1 != null &&
        pin2 != null &&
        pin3 != null &&
        pin4 != null &&
        pin5 != null &&
        pin6 != null) {
      string = pin1! + pin2! + pin3! + pin4! + pin5! + pin6!;
    }
    return string;
  }

  Future<void> hasInternetMethod() async {
    hasInternet = await InternetConnectionChecker().hasConnection;
    if (hasInternet == false) {
      showAboutDialog(
          context: context,
          applicationName: 'GyMatch',
          applicationIcon: SvgPicture.asset('assets/images/close_icon.svg'),
          children: <Widget>[const Text('Check your internet connection.')]);
    }
    setState(() => isLoading = false);
    setState(() => isLoading2 = false);
  }

  Future<String> getEmail() async {
    final prefs = await SharedPreferences.getInstance();
    final email = prefs.getString('email') ?? '';
    return email;
  }

  Future<void> getEmailVerificationInfo() async {
    otp = getOtp();
    email = await getEmail();
    var response = await _apiBaseHelper.postHTTPDev('/validate-otp',
        {"email": email, "otp": otp, "reintentar": reintentar});
    if (response.data != null &&
        response.status == 200 &&
        reintentar == false) {
      isCode = true;
      showAboutDialog(
          context: context,
          applicationName: 'GyMatch',
          applicationIcon: SvgPicture.asset('assets/images/checkmark_icon.svg'),
          children: <Widget>[const Text('Account created succesfully.')]);
      setState(() => isLoading = false);
    } else if (response.data != null &&
        response.status == 200 &&
        reintentar == true) {
      reintentar = false;
      showAboutDialog(
          context: context,
          applicationName: 'GyMatch',
          applicationIcon: SvgPicture.asset('assets/images/checkmark_icon.svg'),
          children: <Widget>[const Text('New code sent, try again.')]);
      setState(() => isLoading2 = false);
    } else {
      setState(() => isLoading = false);
    }
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Scaffold(
        body: SizedBox(
            width: double.infinity,
            height: size.height,
            child: Form(
              child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    const TextFieldContainer(
                        key: null,
                        child: Text('GyMatch',
                            textAlign: TextAlign.center,
                            style: TextStyle(
                                fontWeight: FontWeight.bold,
                                fontSize: 60,
                                color: Color.fromRGBO(87, 131, 188, 1.0)))),
                    const TextFieldContainer(
                        key: null,
                        child: Text('Verify email',
                            textAlign: TextAlign.center,
                            style: TextStyle(
                                fontWeight: FontWeight.bold, fontSize: 30))),
                    Form(
                        key: _formKey,
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceAround,
                          children: [
                            SizedBox(
                              height: 68,
                              width: 64,
                              child: TextFormField(
                                  onChanged: (value) {
                                    if (value.length == 1) {
                                      FocusScope.of(context).nextFocus();
                                    }
                                  },
                                  onSaved: (pin1) {},
                                  decoration:
                                      const InputDecoration(hintText: "0"),
                                  style: Theme.of(context).textTheme.headline6,
                                  keyboardType: TextInputType.number,
                                  textAlign: TextAlign.center,
                                  inputFormatters: [
                                    LengthLimitingTextInputFormatter(1),
                                    FilteringTextInputFormatter.digitsOnly,
                                  ],
                                  validator: (val) {
                                    pin1 = val;
                                    if (val!.isEmpty == true) {
                                      return 'Please, type the full code.';
                                    } else {
                                      return null;
                                    }
                                  }),
                            ),
                            SizedBox(
                              height: 68,
                              width: 64,
                              child: TextFormField(
                                  onChanged: (value) {
                                    if (value.length == 1) {
                                      FocusScope.of(context).nextFocus();
                                    }
                                  },
                                  onSaved: (pin2) {},
                                  decoration:
                                      const InputDecoration(hintText: "0"),
                                  style: Theme.of(context).textTheme.headline6,
                                  keyboardType: TextInputType.number,
                                  textAlign: TextAlign.center,
                                  inputFormatters: [
                                    LengthLimitingTextInputFormatter(1),
                                    FilteringTextInputFormatter.digitsOnly,
                                  ],
                                  validator: (val) {
                                    pin2 = val;
                                    if (val!.isEmpty == true) {
                                      return 'Please, type the full code.';
                                    } else {
                                      return null;
                                    }
                                  }),
                            ),
                            SizedBox(
                              height: 68,
                              width: 64,
                              child: TextFormField(
                                  onChanged: (value) {
                                    if (value.length == 1) {
                                      FocusScope.of(context).nextFocus();
                                    }
                                  },
                                  onSaved: (pin3) {},
                                  decoration:
                                      const InputDecoration(hintText: "0"),
                                  style: Theme.of(context).textTheme.headline6,
                                  keyboardType: TextInputType.number,
                                  textAlign: TextAlign.center,
                                  inputFormatters: [
                                    LengthLimitingTextInputFormatter(1),
                                    FilteringTextInputFormatter.digitsOnly,
                                  ],
                                  validator: (val) {
                                    pin3 = val;
                                    if (val!.isEmpty == true) {
                                      return 'Please, type the full code.';
                                    } else {
                                      return null;
                                    }
                                  }),
                            ),
                            SizedBox(
                              height: 68,
                              width: 64,
                              child: TextFormField(
                                  onChanged: (value) {
                                    if (value.length == 1) {
                                      FocusScope.of(context).nextFocus();
                                    }
                                  },
                                  onSaved: (pin4) {},
                                  decoration:
                                      const InputDecoration(hintText: "0"),
                                  style: Theme.of(context).textTheme.headline6,
                                  keyboardType: TextInputType.number,
                                  textAlign: TextAlign.center,
                                  inputFormatters: [
                                    LengthLimitingTextInputFormatter(1),
                                    FilteringTextInputFormatter.digitsOnly,
                                  ],
                                  validator: (val) {
                                    pin4 = val;
                                    if (val!.isEmpty == true) {
                                      return 'Please, type the full code.';
                                    } else {
                                      return null;
                                    }
                                  }),
                            ),
                            SizedBox(
                              height: 68,
                              width: 64,
                              child: TextFormField(
                                  onChanged: (value) {
                                    if (value.length == 1) {
                                      FocusScope.of(context).nextFocus();
                                    }
                                  },
                                  onSaved: (pin5) {},
                                  decoration:
                                      const InputDecoration(hintText: "0"),
                                  style: Theme.of(context).textTheme.headline6,
                                  keyboardType: TextInputType.number,
                                  textAlign: TextAlign.center,
                                  inputFormatters: [
                                    LengthLimitingTextInputFormatter(1),
                                    FilteringTextInputFormatter.digitsOnly,
                                  ],
                                  validator: (val) {
                                    pin5 = val;
                                    if (val!.isEmpty == true) {
                                      return 'Please, type the full code.';
                                    } else {
                                      return null;
                                    }
                                  }),
                            ),
                            SizedBox(
                              height: 68,
                              width: 64,
                              child: TextFormField(
                                  onSaved: (pin6) {},
                                  decoration:
                                      const InputDecoration(hintText: "0"),
                                  style: Theme.of(context).textTheme.headline6,
                                  keyboardType: TextInputType.number,
                                  textAlign: TextAlign.center,
                                  inputFormatters: [
                                    LengthLimitingTextInputFormatter(1),
                                    FilteringTextInputFormatter.digitsOnly,
                                  ],
                                  validator: (val) {
                                    pin6 = val;
                                    if (val!.isEmpty == true) {
                                      return 'Please, type the full code.';
                                    } else {
                                      return null;
                                    }
                                  }),
                            )
                          ],
                        )),
                    Row(
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        children: [
                          Flexible(
                            flex: 1,
                            fit: FlexFit.tight,
                            child: ClipRRect(
                              borderRadius: BorderRadius.circular(29),
                              child: ElevatedButton(
                                style: ButtonStyle(
                                    backgroundColor:
                                        MaterialStateProperty.all<Color>(
                                            const Color.fromRGBO(
                                                169, 169, 169, 1.0))),
                                child: isLoading2
                                    ? const CircularProgressIndicator(
                                        color: Colors.white, strokeWidth: 2.0)
                                    : const Text('Send again',
                                        style: TextStyle(color: Colors.white)),
                                onPressed: () async {
                                  reintentar = true;
                                  setState(() => isLoading2 = true);
                                  await hasInternetMethod();
                                  await getEmailVerificationInfo();
                                },
                              ),
                            ),
                          ),
                          Flexible(
                            flex: 1,
                            fit: FlexFit.tight,
                            child: ClipRRect(
                              borderRadius: BorderRadius.circular(29),
                              child: ElevatedButton(
                                style: ButtonStyle(
                                    backgroundColor:
                                        MaterialStateProperty.all<Color>(
                                            const Color.fromRGBO(
                                                87, 131, 188, 1.0))),
                                child: isLoading
                                    ? const CircularProgressIndicator(
                                        color: Colors.white, strokeWidth: 2.0)
                                    : const Text('Verify',
                                        style: TextStyle(color: Colors.white)),
                                onPressed: () async {
                                  if (!_formKey.currentState!.validate()) {
                                    return;
                                  }
                                  setState(() => isLoading = true);
                                  await hasInternetMethod();
                                  await getEmailVerificationInfo();
                                  if (isCode == true) {
                                    Navigator.push(
                                      context,
                                      MaterialPageRoute(builder: (context) {
                                        return const SigninScreen();
                                      }),
                                    );
                                  } else if (isCode == false &&
                                      hasInternet == true) {
                                    showAboutDialog(
                                        context: context,
                                        applicationName: 'GyMatch',
                                        applicationIcon: SvgPicture.asset(
                                            'assets/images/close_icon.svg'),
                                        children: <Widget>[
                                          const Text('Invalid code.')
                                        ]);
                                  }
                                },
                              ),
                            ),
                          )
                        ]),
                  ]),
            )));
  }
}

class TextFieldContainer extends StatelessWidget {
  final Widget child;
  const TextFieldContainer({
    required Key? key,
    required this.child,
  }) : super(key: key);
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Container(
      margin: const EdgeInsets.symmetric(vertical: 10),
      padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 5),
      width: size.width * 0.8,
      decoration: BoxDecoration(
          color: Colors.white, borderRadius: BorderRadius.circular(29)),
      child: child,
    );
  }
}
