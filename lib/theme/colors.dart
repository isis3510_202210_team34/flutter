import 'package:flutter/material.dart';

const Color white = Colors.white;
const Color grey = Colors.grey;
const Color black = Colors.black;
const Color green = Colors.green;
const Color red = Colors.red;

const Color blueOne = Color.fromARGB(255, 33, 140, 228);
const Color blueTwo = Color.fromARGB(255, 98, 185, 255);
