const List item_icons = [
  {"icon": "assets/images/refresh_icon.svg", "size": 50.0, "icon_size": 25.0},
  {"icon": "assets/images/close_icon.svg", "size": 50.0, "icon_size": 28.0},
  {"icon": "assets/images/star_icon.svg", "size": 50.0, "icon_size": 25.0},
  {"icon": "assets/images/checkmark_icon.svg", "size": 50.0, "icon_size": 29.0},
];
