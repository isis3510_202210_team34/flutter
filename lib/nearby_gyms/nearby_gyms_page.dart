import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:gym_match/entities/gym_location.dart';
import 'package:gym_match/nearby_gyms/bloc/nearby_gyms_bloc.dart';
import 'package:gym_match/nearby_gyms/widgets/bottom_sheet_marker_gym.dart';
import 'package:gym_match/nearby_gyms/widgets/detail_user_gym.dart';
import 'package:gym_match/nearby_gyms/widgets/map_view_gym.dart';
import 'package:gym_match/utils/theme.dart';
import 'package:gym_match/widgets/logo.dart';
import 'package:intl/intl.dart' as intl;
import 'package:gym_match/utils/responsive.dart';

class NearbyGymsPage extends StatefulWidget {
  NearbyGymsPage({Key? key}) : super(key: key);

  final GlobalKey<ScaffoldState> _scaffoldkey = new GlobalKey<ScaffoldState>();

  @override
  _NearbyGymsPageState createState() => _NearbyGymsPageState();
}

class _NearbyGymsPageState extends State<NearbyGymsPage> {
  late NearbyGymsBloc nearbyGymBloc;
  @override
  void initState() {
    super.initState();
    nearbyGymBloc = BlocProvider.of<NearbyGymsBloc>(context);
    nearbyGymBloc.getCurrentPosition(openBottomSheetMarker);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: widget._scaffoldkey,
      body: BlocBuilder<NearbyGymsBloc, NearbyGymState>(
        builder: (context, state) {
          if (state.myLocation == null) {
            return const Center(child: CircularProgressIndicator());
          }

          LatLng position = state.myLocation!;

          return SizedBox(
            height: Size.infinite.height,
            child: Stack(
              children: [
                Positioned(
                  top: 0.h,
                  bottom: 0.h,
                  child: SizedBox(
                    height: 82.h,
                    width: 100.w,
                    child: MapViewGyms(
                        initialLocation: position,
                        customMarkers: state.customMarkers!),
                  ),
                ),
                Container(
                  height: ResponsiveUtil.zoom < 1.2 ? 30.h : 35.h,
                  decoration: const BoxDecoration(
                      color: GymMatchTheme.backGround,
                      borderRadius: BorderRadius.only(
                          bottomLeft: Radius.circular(40),
                          bottomRight: Radius.circular(40))),
                ),
                const Logo(),
                Positioned(
                  top: 6.h,
                  left: 4.w,
                  child: GestureDetector(
                      onTap: () => Navigator.pop(context),
                      child: Icon(
                        Icons.arrow_back,
                        color: GymMatchTheme.white,
                        size: 9.w,
                      )),
                ),
                Positioned(
                  top: 12.h,
                  left: 4.w,
                  right: 4.w,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          DetailUserGym(
                            title: state.user!.names,
                            icon: Icons.person,
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
                Positioned(
                  top: ResponsiveUtil.zoom < 1.2 ? 26.h : 25.h,
                  left: 4.w,
                  right: 4.w,
                  child: const Center(
                    child: Text(
                      "¡Estos gimansios estan cerca de tí!",
                      overflow: TextOverflow.clip,
                      textAlign: TextAlign.center,
                      style: GymMatchTheme.textStyle2h,
                    ),
                  ),
                ),
              ],
            ),
          );
        },
      ),
    );
  }

  void openBottomSheetMarker(GymModel gym) {
    if (widget._scaffoldkey.currentContext != null) {
      showModalBottomSheet(
        backgroundColor: Colors.transparent,
        context: widget._scaffoldkey.currentContext!,
        builder: (context) {
          return BottomSheetMarkerGym(
            gym: gym,
          );
        },
      );
    }
  }
}
