import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:geolocator/geolocator.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart'
    show LatLng, Marker, MarkerId;
import 'package:gym_match/entities/gym_location.dart';
import 'package:gym_match/entities/user.dart';
import 'package:gym_match/nearby_gyms/data/nearby_gyms_repository_iml.dart';
import 'package:hive/hive.dart';

part 'nearby_gyms_event.dart';
part 'nearby_gyms_state.dart';

class NearbyGymsBloc extends Bloc<NearbyGymEvent, NearbyGymState> {
  final NearbyGymsRepositoryIml _nearbyGymRepositoryIml;
  StreamSubscription<Position>? positionStream;
  List<Marker> customMarkers = [];

  NearbyGymsBloc(this._nearbyGymRepositoryIml) : super(const NearbyGymState()) {
    on<OnNewUserLocationEvent>((event, emit) async {
      emit(state.copyWith(
          myLocation: event.newLocation,
          customMarkers: event.customMarkers,
          user: event.user));
    });
  }

  Future getCurrentPosition(Function openMarker) async {
    final position = await Geolocator.getCurrentPosition();

    var box = await Hive.openBox<UserModel>('user');
    var apiResults = await _nearbyGymRepositoryIml.getGymsInfo(
        position.latitude, position.longitude);
    if (apiResults.data != null) {
      for (GymModel gym in apiResults.data) {
        customMarkers.add(Marker(
          onTap: () => openMarker(gym),
          markerId: MarkerId(gym.id.toString()),
          position: LatLng(gym.latitude, gym.longitude),
        ));
      }
      add(OnNewUserLocationEvent(LatLng(position.latitude, position.longitude),
          customMarkers, box.get('user')!));
    } else {}
  }
}
