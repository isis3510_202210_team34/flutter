part of 'nearby_gyms_bloc.dart';

class NearbyGymState extends Equatable {
  final LatLng? myLocation;
  final List<Marker>? customMarkers;
  final UserModel? user;

  const NearbyGymState({this.customMarkers, this.myLocation, this.user});

  NearbyGymState copyWith({
    List<Marker>? customMarkers,
    LatLng? myLocation,
    int? temperature,
    UserModel? user,
  }) =>
      NearbyGymState(
          customMarkers: customMarkers ?? this.customMarkers,
          myLocation: myLocation ?? this.myLocation,
          user: user ?? this.user);

  @override
  List<Object?> get props => [myLocation, customMarkers, user];
}
