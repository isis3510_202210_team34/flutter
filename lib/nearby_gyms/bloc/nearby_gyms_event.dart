part of 'nearby_gyms_bloc.dart';

abstract class NearbyGymEvent extends Equatable {
  const NearbyGymEvent();

  @override
  List<Object> get props => [];
}

class OnNewUserLocationEvent extends NearbyGymEvent {
  final LatLng newLocation;
  final List<Marker> customMarkers;
  final UserModel user;
  const OnNewUserLocationEvent(this.newLocation, this.customMarkers, this.user);
}
