import 'package:gym_match/entities/api_response.dart';
import 'package:gym_match/entities/gym_location.dart';
import 'package:gym_match/nearby_gyms/data/nearby_gyms_repository_iml.dart';
import 'package:gym_match/utils/api_helper.dart';

class NearbyGymsRepository extends NearbyGymsRepositoryIml {
  final ApiBaseHelper _apiBaseHelper;

  NearbyGymsRepository(this._apiBaseHelper);
  @override
  Future<ApiResponse> getGymsInfo(double lat, double long) async {
    var response = await _apiBaseHelper.postHTTPDev('/get-near-gyms', {
      "latitude": lat,
      "longitude": long,
    });

    if (response.data != null && response.status == 200) {
      response.data =
          (response.data as List).map((i) => GymModel.fromJson(i)).toList();
      return response;
    }
    return response;
  }
}
