import 'package:flutter/material.dart';
import 'package:gym_match/utils/responsive.dart';
import 'package:gym_match/utils/theme.dart';

class DetailUserGym extends StatelessWidget {
  const DetailUserGym({
    Key? key,
    required this.title,
    required this.icon,
  }) : super(key: key);

  final String title;
  final IconData icon;

  @override
  Widget build(BuildContext context) {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.center,
      mainAxisAlignment: MainAxisAlignment.start,
      children: [
        Icon(
          icon,
          color: GymMatchTheme.yellow,
          size: 8.w,
        ),
        SizedBox(
          width: 1.w,
        ),
        Text(
          title,
          overflow: TextOverflow.clip,
          style: GymMatchTheme.textStyle
              .copyWith(fontSize: ResponsiveUtil.zoom < 1.2 ? 2.5.h : 2.h),
        ),
      ],
    );
  }
}
