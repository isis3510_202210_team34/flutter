import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:gym_match/entities/gym_location.dart';
import 'package:gym_match/utils/responsive.dart';
import '../../utils/theme.dart';

class BottomSheetMarkerGym extends StatelessWidget {
  const BottomSheetMarkerGym({Key? key, required this.gym}) : super(key: key);

  final GymModel gym;

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 30.h,
      margin: const EdgeInsets.all(16.0),
      clipBehavior: Clip.antiAlias,
      decoration: const BoxDecoration(
        color: GymMatchTheme.white,
        borderRadius: BorderRadius.all(Radius.circular(16.0)),
      ),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          _buildHandle(context),
          Padding(
            padding: EdgeInsets.only(top: 1.h, bottom: 2.h),
            child: Text(gym.name,
                overflow: TextOverflow.clip,
                textAlign: TextAlign.center,
                style: GymMatchTheme.textStyle3h.copyWith(
                  color: GymMatchTheme.backGround,
                  fontSize: ResponsiveUtil.zoom < 1.2 ? 2.5.h : 2.h,
                )),
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Container(
              decoration: BoxDecoration(
                  color: GymMatchTheme.miniContainer,
                  borderRadius: BorderRadius.circular(16)),
              child: Padding(
                padding: EdgeInsets.only(
                    top: 2.h, left: 1.w, right: 1.w, bottom: 2.h),
                child: Column(
                  children: [
                    Text('Rating: ${gym.rating}',
                        overflow: TextOverflow.clip,
                        textAlign: TextAlign.center,
                        style: GymMatchTheme.textStyle3h.copyWith(
                          color: GymMatchTheme.white,
                          fontSize: ResponsiveUtil.zoom < 1.2 ? 2.2.h : 1.8.h,
                        )),
                    Text(
                        'Distancia desde donde estas: ${gym.distanceKm.toStringAsFixed(3)} KM',
                        overflow: TextOverflow.clip,
                        textAlign: TextAlign.center,
                        style: GymMatchTheme.textStyle3h.copyWith(
                          color: GymMatchTheme.white,
                          fontSize: ResponsiveUtil.zoom < 1.2 ? 2.2.h : 1.8.h,
                        )),
                    GestureDetector(
                      child: Tooltip(
                        preferBelow: false,
                        message: "Copiado",
                        child: Text('Dirección: ${gym.address}',
                            overflow: TextOverflow.clip,
                            textAlign: TextAlign.center,
                            style: GymMatchTheme.textStyle3h.copyWith(
                              color: GymMatchTheme.white,
                              fontSize:
                                  ResponsiveUtil.zoom < 1.2 ? 2.2.h : 1.8.h,
                            )),
                      ),
                      onTap: () {
                        Clipboard.setData(ClipboardData(text: gym.address));
                      },
                    ),
                  ],
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget _buildHandle(BuildContext context) {
    return FractionallySizedBox(
      widthFactor: 0.25,
      child: Container(
        margin: const EdgeInsets.symmetric(
          vertical: 12.0,
        ),
        child: Container(
          height: 0.5.h,
          decoration: const BoxDecoration(
            color: GymMatchTheme.gray,
            borderRadius: BorderRadius.all(Radius.circular(2.5)),
          ),
        ),
      ),
    );
  }
}
