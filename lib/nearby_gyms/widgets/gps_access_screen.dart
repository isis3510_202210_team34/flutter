import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:gym_match/nearby_gyms/bloc/gps/gps_bloc.dart';
import 'package:gym_match/utils/theme.dart';
import 'package:gym_match/utils/assets.dart';
import 'package:gym_match/utils/responsive.dart';

class GpsAccessScreen extends StatelessWidget {
  const GpsAccessScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: GymMatchTheme.backGround,
      body: BlocBuilder<GpsBloc, GpsState>(
        builder: (context, state) {
          return !state.isGpsEnabled
              ? const _EnableGpsMessage()
              : const _AccessButton();
        },
      ),
    );
  }
}

class _AccessButton extends StatelessWidget {
  const _AccessButton({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.spaceAround,
      children: [
        Align(
          alignment: Alignment.topLeft,
          child: GestureDetector(
            onTap: () => Navigator.pop(context),
            child: Icon(
              Icons.arrow_back,
              color: GymMatchTheme.white,
              size: 9.w,
            ),
          ),
        ),
        Image.asset(
          requestGPS,
          height: 50.h,
        ),
        const Text(
            'Necesitamos tu ubicación para mostrate las sucursales cercanas a ti',
            overflow: TextOverflow.clip,
            style: GymMatchTheme.textStyle,
            textAlign: TextAlign.center),
        GestureDetector(
          onTap: () {
            final gpsBloc = BlocProvider.of<GpsBloc>(context);
            gpsBloc.askGpsAccess();
          },
          child: Container(
              width: 30.h,
              decoration: const BoxDecoration(
                color: GymMatchTheme.white,
                borderRadius: BorderRadius.all(Radius.circular(10)),
                boxShadow: [
                  BoxShadow(
                    color: Color(0xff000000),
                    offset: Offset(2, 2),
                    blurRadius: 2,
                    spreadRadius: 0.5,
                  ),
                ],
              ),
              alignment: Alignment.center,
              child: Padding(
                padding: EdgeInsets.all(2.h),
                child: const Center(
                    child: Text('Solicitar Acceso',
                        overflow: TextOverflow.clip,
                        style: GymMatchTheme.textStyle,
                        textAlign: TextAlign.center)),
              )),
        ),
      ],
    );
  }
}

class _EnableGpsMessage extends StatelessWidget {
  const _EnableGpsMessage({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Image.asset(
            requestGPS,
            height: 50.h,
          ),
          const Text(
            'Por favor Habilita tu ubicación',
            overflow: TextOverflow.clip,
            style: GymMatchTheme.textStyle3h,
          ),
        ],
      ),
    );
  }
}
