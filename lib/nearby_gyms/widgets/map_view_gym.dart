import 'dart:async';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:gym_match/nearby_gyms/bloc/map/map_bloc.dart';

class MapViewGyms extends StatelessWidget {
  final LatLng initialLocation;
  final List<Marker> customMarkers;

  const MapViewGyms(
      {Key? key, required this.initialLocation, required this.customMarkers})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    final mapBloc = BlocProvider.of<MapBloc>(context);
    Completer<GoogleMapController> mapController = Completer();

    final CameraPosition initialCameraPosition =
        CameraPosition(target: initialLocation, zoom: 18);

    return GoogleMap(
      markers: customMarkers.toSet(),
      initialCameraPosition: initialCameraPosition,
      myLocationEnabled: true,
      onMapCreated: (controller) {
        mapBloc.add(OnMapInitialzedEvent(controller));
        mapController.complete(controller);
      },
    );
  }
}
