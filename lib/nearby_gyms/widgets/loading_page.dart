import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:gym_match/nearby_gyms/bloc/gps/gps_bloc.dart';
import 'package:gym_match/nearby_gyms/nearby_gyms_page.dart';
import 'package:gym_match/nearby_gyms/widgets/gps_access_screen.dart';

class ValidatePermission extends StatelessWidget {
  const ValidatePermission({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(body: BlocBuilder<GpsBloc, GpsState>(
      builder: (context, state) {
        return state.isAllGranted ? NearbyGymsPage() : const GpsAccessScreen();
      },
    ));
  }
}
