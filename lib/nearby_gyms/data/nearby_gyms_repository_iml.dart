import 'package:gym_match/entities/api_response.dart';

abstract class NearbyGymsRepositoryIml {
  Future<ApiResponse> getGymsInfo(double lat, double long);
}
