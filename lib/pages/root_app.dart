import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

import 'package:gym_match/pages/likes_page.dart';
import 'package:gym_match/theme/colors.dart';

class RootApp extends StatefulWidget {
  const RootApp({Key? key}) : super(key: key);

  @override
  _RootAppState createState() => _RootAppState();
}

class _RootAppState extends State<RootApp> {
  int pageIndex = 0;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: white,
      bottomNavigationBar: getAppBar(),
      body: getBody(),
    );
  }

  Widget getBody() {
    return IndexedStack(
      index: pageIndex,
      children: const [LikesPage(), LikesPage(), LikesPage(), LikesPage()],
    );
  }

  PreferredSizeWidget getAppBar() {
    var items = [
      pageIndex == 0
          ? "assets/images/dumbbell_active_icon.svg"
          : "assets/images/dumbbell_icon.svg",
      pageIndex == 1
          ? "assets/images/heart_active_icon.svg"
          : "assets/images/heart_icon.svg",
      pageIndex == 2
          ? "assets/images/chat_active_icon.svg"
          : "assets/images/chat_icon.svg",
      pageIndex == 3
          ? "assets/images/account_active_icon.svg"
          : "assets/images/account_icon.svg",
    ];

    return AppBar(
        backgroundColor: white,
        elevation: 0,
        title: Padding(
          padding: const EdgeInsets.only(left: 10, right: 10),
          child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: List.generate(items.length, (index) {
                    return IconButton(
                      onPressed: () {
                        setState(() {
                          pageIndex = index;
                        });
                      },
                      icon: SvgPicture.asset(items[index]),
                    );
                  }),
                ),
              ]),
        ));
  }
}
