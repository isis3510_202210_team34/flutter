import 'package:gym_match/nearby_gyms/bloc/nearby_gyms_bloc.dart';
import 'package:gym_match/nearby_gyms/data/nearby_gyms_repository_iml.dart';
import 'package:gym_match/nearby_gyms/repository/nearby_gyms_repository.dart';
import 'package:gym_match/utils/api_helper.dart';
import 'package:injector/injector.dart';

class Dependencies {
  final injector = Injector.appInstance;

  blocsRegister() {
    injector.registerSingleton<NearbyGymsBloc>(
        () => NearbyGymsBloc(injector.get<NearbyGymsRepositoryIml>()));
  }

  repositoryRegister() {
    injector.registerDependency<NearbyGymsRepositoryIml>(
        () => NearbyGymsRepository(injector.get<ApiBaseHelper>()));
  }
}
