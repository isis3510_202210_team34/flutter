import 'package:flutter/material.dart';

class GymMatchTheme {
  static const Color black = Color(0xff000000);
  static const Color white = Color(0xffffffff);
  static const Color backGround = white;
  static const Color gray = Color.fromRGBO(70, 70, 70, 1);
  static const Color secondGray = Color.fromRGBO(90, 96, 116, 1);
  static const Color miniContainer = Color.fromRGBO(147, 130, 255, 1);
  static const Color error = Color(0xffD96C5D);
  static const Color yellow = Color(0xfff3ce3c);

  static const TextStyle textStyle = TextStyle(
      fontFamily: 'OpenSans',
      color: GymMatchTheme.white,
      fontSize: 20,
      fontWeight: FontWeight.bold);
  static const TextStyle textStyle2h = TextStyle(
      fontFamily: 'OpenSans',
      color: GymMatchTheme.white,
      fontSize: 15,
      fontWeight: FontWeight.bold);
  static const TextStyle textStyle3h = TextStyle(
      fontFamily: 'OpenSans',
      color: GymMatchTheme.white,
      fontSize: 25,
      fontWeight: FontWeight.bold);
  static const TextStyle textStyleError = TextStyle(
      fontFamily: 'OpenSans',
      color: GymMatchTheme.white,
      fontSize: 15,
      fontWeight: FontWeight.bold);
}
