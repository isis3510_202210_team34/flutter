import 'package:gym_match/nearby_gyms/nearby_gyms_page.dart';
import 'package:gym_match/nearby_gyms/widgets/loading_page.dart';
import 'package:gym_match/pages/root_app.dart';
//import 'package:gym_match/pages/root_app.dart';
import 'package:gym_match/sign_up/emailVerification.dart';
import 'package:gym_match/sign_up/sign_up_screen.dart';
import 'package:gym_match/sign_in/sign_in_screen.dart';

class MyRoutes {
  static final routes = {
    'sign_up': (context) => const SignupScreen(),
    'sign_in': (context) => const SigninScreen(),
    'otp': (context) => const EmailVerificationScreen(),
    'home': (context) => const RootApp(),
    'validate_permission': (context) => const ValidatePermission(),
    'nearby_gyms': (context) => NearbyGymsPage(),
  };
}
