import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_config/flutter_config.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:gym_match/entities/user.dart';
import 'package:gym_match/nearby_gyms/bloc/gps/gps_bloc.dart';
import 'package:gym_match/nearby_gyms/bloc/nearby_gyms_bloc.dart';
import 'package:gym_match/sign_up/sign_up_screen.dart';
import 'package:gym_match/utils/injector.dart';
import 'package:gym_match/utils/json_reader.dart';
import 'package:gym_match/utils/router.dart';
import 'package:hive/hive.dart';
import 'package:injector/injector.dart';
import 'package:path_provider/path_provider.dart' as path;

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await FlutterConfig.loadEnvVariables();
  //await Firebase.initializeApp();
  final appDocumentDir = await path.getApplicationDocumentsDirectory();
  SystemChrome.setPreferredOrientations(
      [DeviceOrientation.portraitUp, DeviceOrientation.portraitDown]);
  Hive
    ..init(appDocumentDir.path)
    ..registerAdapter(UserModelAdapter());
  await Hive.openBox('isLogged');
  JsonReader.initialize();
  Dependencies().repositoryRegister();
  Dependencies().blocsRegister();
  runApp(const MyApp());
}

class MyApp extends StatefulWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  State<MyApp> createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        BlocProvider(create: (_) => Injector.appInstance.get<GpsBloc>()),
        BlocProvider(
            create: (context) => Injector.appInstance.get<NearbyGymsBloc>()),
      ],
      child: MaterialApp(
        locale: const Locale('es'),
        localizationsDelegates: const [
          GlobalMaterialLocalizations.delegate,
          GlobalCupertinoLocalizations.delegate,
          GlobalWidgetsLocalizations.delegate,
        ],
        supportedLocales: const [Locale('es')],
        debugShowCheckedModeBanner: false,
        routes: MyRoutes.routes,
        home: const SignupScreen(),
      ),
    );
  }
}
