import 'package:flutter/material.dart';
import 'package:gym_match/utils/assets.dart';
import 'package:gym_match/utils/responsive.dart';

class Logo extends StatelessWidget {
  const Logo({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Positioned(
        top: 3.h,
        right: 4.w,
        child: GestureDetector(
          onTap: () {
            Navigator.of(context).pushNamed('about_us');
          },
          child: Image.asset(
            gymIcon,
            height: 10.h,
            width: 10.h,
          ),
        ));
  }
}
