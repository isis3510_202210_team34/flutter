import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:flutter/services.dart';
import 'package:gym_match/nearby_gyms/nearby_gyms_page.dart';
import 'package:gym_match/pages/root_app.dart';
import 'package:gym_match/sign_up/sign_up_screen.dart';
import 'package:gym_match/utils/api_helper.dart';
import 'package:internet_connection_checker/internet_connection_checker.dart';

class SigninScreen extends StatefulWidget {
  const SigninScreen({Key? key}) : super(key: key);

  @override
  _SigninScreen createState() => _SigninScreen();
}

class _SigninScreen extends State<SigninScreen> {
  final _formKey = GlobalKey<FormState>();
  final _apiBaseHelper = ApiBaseHelper();
  bool isLoading = false;
  bool hasInternet = true;
  String? email;
  String? password;
  bool isUser = false;

  Future<void> hasInternetMethod() async {
    hasInternet = await InternetConnectionChecker().hasConnection;
    if (hasInternet == false) {
      showAboutDialog(
          context: context,
          applicationName: 'GyMatch',
          applicationIcon: SvgPicture.asset('assets/images/close_icon.svg'),
          children: <Widget>[const Text('Check your internet connection.')]);
    }
    setState(() => isLoading = false);
  }

  Future<void> getSigninInfo() async {
    var response = await _apiBaseHelper
        .postHTTPDev('/sign-in', {"email": email, "password": password});
    if (response.data != null && response.status == 200) {
      isUser = true;
      setState(() => isLoading = false);
    } else {
      setState(() => isLoading = false);
    }
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Scaffold(
      body: SizedBox(
        width: double.infinity,
        height: size.height,
        child: Form(
          key: _formKey,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              const TextFieldContainer(
                  key: null,
                  child: Text('GyMatch',
                      textAlign: TextAlign.center,
                      style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 60,
                          color: Color.fromRGBO(87, 131, 188, 1.0)))),
              const TextFieldContainer(
                  key: null,
                  child: Text('Sign in',
                      textAlign: TextAlign.center,
                      style: TextStyle(
                          fontWeight: FontWeight.bold, fontSize: 30))),
              TextFieldContainer(
                  key: null,
                  child: TextFormField(
                    keyboardType: TextInputType.emailAddress,
                    decoration: const InputDecoration(
                      hintText: "Email address",
                    ),
                    inputFormatters: [
                      FilteringTextInputFormatter.singleLineFormatter,
                    ],
                    onSaved: (val) {},
                    validator: (val) {
                      email = val;
                      if (val!.isEmpty == true) {
                        return 'Please, type your email address.';
                      } else if (val.contains('@') == false ||
                          val.contains('.') == false) {
                        return 'That is not a valid email address.';
                      } else {
                        return null;
                      }
                    },
                  )),
              TextFieldContainer(
                key: null,
                child: TextFormField(
                    keyboardType: TextInputType.number,
                    obscureText: true,
                    decoration: const InputDecoration(
                      hintText: "Password",
                    ),
                    inputFormatters: [
                      LengthLimitingTextInputFormatter(6),
                      FilteringTextInputFormatter.digitsOnly,
                    ],
                    onSaved: (val) {},
                    validator: (val) {
                      password = val;
                      if (val!.length < 6) {
                        return 'The password is too short.';
                      } else if (val.isEmpty == true) {
                        return 'Please, type your password.';
                      } else {
                        return null;
                      }
                    }),
              ),
              Row(mainAxisAlignment: MainAxisAlignment.spaceEvenly, children: [
                Flexible(
                  flex: 1,
                  fit: FlexFit.tight,
                  child: ClipRRect(
                    borderRadius: BorderRadius.circular(29),
                    child: ElevatedButton(
                      style: ButtonStyle(
                          backgroundColor: MaterialStateProperty.all<Color>(
                              const Color.fromRGBO(169, 169, 169, 1.0))),
                      onPressed: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(builder: (context) {
                            return const SignupScreen();
                          }),
                        );
                      },
                      child: const Text('Create an account',
                          style: TextStyle(color: Colors.white)),
                    ),
                  ),
                ),
                Flexible(
                  flex: 1,
                  fit: FlexFit.tight,
                  child: ClipRRect(
                    borderRadius: BorderRadius.circular(29),
                    child: ElevatedButton(
                      style: ButtonStyle(
                          backgroundColor: MaterialStateProperty.all<Color>(
                              const Color.fromRGBO(87, 131, 188, 1.0))),
                      child: isLoading
                          ? const CircularProgressIndicator(
                              color: Colors.white, strokeWidth: 2.0)
                          : const Text('SIGN IN',
                              style: TextStyle(color: Colors.white)),
                      onPressed: () async {
                        if (!_formKey.currentState!.validate()) {
                          return;
                        }
                        setState(() => isLoading = true);
                        await hasInternetMethod();
                        await getSigninInfo();
                        if (isUser == true) {
                          Navigator.push(
                            context,
                            MaterialPageRoute(builder: (context) {
                              return const RootApp();
                            }),
                          );
                        } else if (isUser == false && hasInternet == true) {
                          showAboutDialog(
                              context: context,
                              applicationName: 'GyMatch',
                              applicationIcon: SvgPicture.asset(
                                  'assets/images/close_icon.svg'),
                              children: <Widget>[
                                const Text('Invalid email or password.')
                              ]);
                        }
                      },
                    ),
                  ),
                )
              ]),
            ],
          ),
        ),
      ),
    );
  }
}

class TextFieldContainer extends StatelessWidget {
  final Widget child;
  const TextFieldContainer({
    required Key? key,
    required this.child,
  }) : super(key: key);
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Container(
      margin: const EdgeInsets.symmetric(vertical: 10),
      padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 5),
      width: size.width * 0.8,
      decoration: BoxDecoration(
          color: Colors.white, borderRadius: BorderRadius.circular(29)),
      child: child,
    );
  }
}
