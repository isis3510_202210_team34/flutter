class GymModel {
  GymModel(
      {required this.id,
      required this.latitude,
      required this.longitude,
      required this.name,
      required this.address,
      required this.imgUrl,
      required this.reviewCount,
      required this.placeUrl,
      required this.rating,
      required this.distanceKm});
  final int id;
  final double latitude;
  final double longitude;
  final String name;
  final String address;
  final String rating;
  final String imgUrl;
  final double reviewCount;
  final String placeUrl;
  final double distanceKm;

  factory GymModel.fromJson(Map<String, dynamic> json) => GymModel(
      id: json["id"],
      name: json["name"],
      latitude: json["latitude"],
      longitude: json['longitude'],
      address: json['address'],
      rating: json["rating"],
      imgUrl: json["img_url"],
      reviewCount: json["review_count"],
      placeUrl: json["place_url"],
      distanceKm: json["distancia_km"]);
}
