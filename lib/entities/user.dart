import 'package:hive/hive.dart';

part 'hive/user.g.dart';

@HiveType(typeId: 1)
class UserModel {
  UserModel(
      {required this.id,
      required this.names,
      required this.familyNames,
      required this.email,
      required this.phoneNumber,
      required this.bornAt,
      required this.gender});

  @HiveField(0)
  String id;
  @HiveField(1)
  String names;
  @HiveField(2)
  String familyNames;
  @HiveField(3)
  String email;
  @HiveField(4)
  String phoneNumber;
  @HiveField(5)
  String bornAt;
  @HiveField(6)
  String gender;

  factory UserModel.fromJson(Map<String, dynamic> json) => UserModel(
      id: json["id"] ?? '',
      names: json["names"] ?? '',
      familyNames: json["family_names"] ?? '',
      email: json["email"] ?? '',
      phoneNumber: json["phone_number"] ?? '',
      bornAt: json["born_at"] ?? '',
      gender: json["gender"] ?? '');

  Map<String, dynamic> toJson() => {
        "id": id,
        "names": names,
        "family_names": familyNames,
        "email": email,
        "phone_number": phoneNumber,
        "born_at": bornAt,
        "gender": gender,
      };
}
